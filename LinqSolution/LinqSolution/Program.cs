﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqSolution
{
    class Program
    {
        static void Main(string[] args)
        {
            var patients = new List<Patient>
            {
                new Patient
                {
                    FirstName = "Jan",
                    LastName = "Kowalski",
                    IsInsured = false,
                    InsuranceType = InsuranceType.Ekuz
                },
                new Patient
                {
                    FirstName = "Adam",
                    LastName = "Nowak",
                    IsInsured = true,
                    InsuranceType = InsuranceType.EWus
                },
                new Patient
                {
                    FirstName = "Anna",
                    LastName = "Zakręcka",
                    IsInsured = true,
                    InsuranceType = InsuranceType.PolishCard
                },
                new Patient
                {
                    FirstName = "Maja",
                    LastName = "Chora",
                    IsInsured = true,
                    InsuranceType = InsuranceType.Private
                }
            };



            // how IEnumerable works
            var enumerator = patients.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Patient patient = enumerator.Current;
            }
            enumerator.Dispose();






            // Without LINQ: get all insured patients with Private insurance
            var filteredPatients = new List<Patient>();
            foreach (Patient patient in patients)
            {
                if (patient.IsInsured && patient.InsuranceType == InsuranceType.Private)
                {
                    filteredPatients.Add(patient);
                }
            }

            foreach (Patient patient in filteredPatients)
            {
                Console.WriteLine($"{patient.FirstName} {patient.LastName}");
            }





            // With LINQ method syntax: get all insured patients with Private insurance
            var filteredPatients2 = 
                patients.Where(patient => patient.IsInsured && patient.InsuranceType == InsuranceType.Private).ToList();





            // Without LINQ query syntax: get all insured patients with Private insurance
            var filteredPatients3 = from patient in patients
                where patient.IsInsured && patient.InsuranceType == InsuranceType.Private
                select patient;      




            foreach (Patient patient in filteredPatients2)
            {
                Console.WriteLine($"{patient.FirstName} {patient.LastName}");
            }
        }



        void LambdaExpressions()
        {
            Func<int, int, int> add = (int x, int y) => x + y;

            Func<int, int, int> add2 = (x, y) => x + y;

            Func<int, int, int> add3 = (x, y) => { return x + y; };

            // result == 5
            int result = add(2, 3);

            Func<int, int> square = (int x) => x * x;

            // result2 == 4
            int result2 = square(2);

            // result 3 == 13
            int result3 = add(square(2), square(3));
        }
    }

    public class Patient
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsInsured { get; set; }
        public InsuranceType InsuranceType { get; set; }
    }

    public enum InsuranceType
    {
        EWus,
        Ekuz,
        Private,
        PolishCard
    }

    public static class PatientSetExtensions
    {
        static IEnumerable<Patient> WhereHandmade<Patient>(this IEnumerable<Patient> collection, Func<Patient, bool> predicate)
        {
            var filteredItems = new List<Patient>();
            foreach (Patient item in collection)
            {
                if (predicate(item))
                {
                    filteredItems.Add(item);
                }
            }

            return filteredItems;
        }
    }
}


